# Nagios Plugin for cPanel License Checking

This is a Nagios plugin to check the status of your cPanel licence

## Requirements

This plugin requires Python 3, as well the `requests` Python module.

## Usage

Using this plugin is quite simple, as it requires only one paramter (`-H / --hostname`)

```bash
# Valid licence
./check_cpanel_license.py -H 192.0.2.1
LICENSE OK - Package: CPDIRECT-PREMIER / Product: cPanel/WHM / Activity: active on 2024-07-04 13:36:06
```

```bash
# Not licensed
./check_cpanel_license.py -H 198.51.100.0
LICENSE CRITICAL - Not licensed
```

Per the [Nagios Plugin Development Guideines](https://nagios-plugins.org/doc/guidelines.html#AEN78),
the plugin will return `0` (OK) if a valid licence is found, `2` (Critical) if no valid licence is found, and
`3` (Unknown) if an invalid hostname is supplied. Return code `1` (Warning) is not currently used.

## Bugs
Bugs can be reported using the [Issue Tracker](https://gitlab.com/ggiesen/nagios-check_cpanel_license/-/issues).

## Contributing
All contributions are welcome and very much appreciated.
Contributing can take many forms, including:

 - Reporting bugs
 - Feature requests
 - Code submissions (bug fixes/new features/improve code quality)
 - Writing tests
 - Writing documentation
 - Detailing use cases
 - Writing blog posts

In particular I'd love to get examples of output (HTML) from the
[cPanel & WHM License Verification portal](https://verify.cpanel.net/app/verify)
as I only have very few examples to work with.

## License
This project is licensed under the Mozilla Public License Version 2.0. See `LICENSE` for the licence text.

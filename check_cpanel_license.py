#!/usr/bin/env python3

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.

# This script is a Nagios plugin to check the status of a cPanel license using the cPanel license page

import argparse

import requests
import socket
import sys
from html.parser import HTMLParser

table_dict = {}


class CPanelHTMLParser(HTMLParser):
    def __init__(self):
        HTMLParser.__init__(self)
        # These variables will tell the parser what data it's reading at the moment
        self.in_table_data = False
        self.found_license_table = False
        self.row_counter = 1
        self.column_counter = 1

    # Called everytime the parser encounters a new tag
    # e.g. <html> or <p>
    def handle_starttag(self, tag, attrs):
        if tag == "table":
            for attr, value in attrs:
                # We've found the history table
                if attr == "class" and value == "history table":
                    self.found_license_table = True
                    break
        if self.found_license_table:
            if tag == "tr":
                table_dict[self.row_counter] = {}
            elif tag == "th":
                self.in_table_data = True
                table_dict[self.row_counter][self.column_counter] = ""
            elif tag == "td":
                self.in_table_data = True
                table_dict[self.row_counter][self.column_counter] = ""
            elif tag == "a":
                pass
            elif tag == "b":
                pass
            elif tag == "br":
                pass
            elif tag == "img":
                pass

    # Called everytime the parser encounters an end tag
    # e.g. </html> or </p>
    def handle_endtag(self, tag):
        if self.found_license_table:
            if tag == "table":
                self.found_license_table = False
            if tag == "tr":
                self.row_counter += 1
                self.column_counter = 1
            elif tag == "th":
                self.in_table_data = False
                self.column_counter += 1
            elif tag == "td":
                self.in_table_data = False
                self.column_counter += 1
            elif tag == "a":
                pass
            elif tag == "b":
                pass
            elif tag == "br":
                pass
            elif tag == "img":
                pass

    # Called when the parser encounters the contents of a tag
    # e.g. 'Some word' in '<p>Some word</p>
    def handle_data(self, data):
        if self.found_license_table and self.in_table_data:
            if not table_dict[self.row_counter][self.column_counter]:
                table_dict[self.row_counter][self.column_counter] = data.replace(
                    "\n", " "
                )
            else:
                table_dict[self.row_counter][self.column_counter] = table_dict[
                    self.row_counter
                ][self.column_counter] + data.replace("\n", " ")


# %s should be put in place of the ip address
LICENSE_CHECK_URL = "https://verify.cpanel.net/app/verify?ip="

# Parse command-line arguments
parser = argparse.ArgumentParser()

parser.add_argument(
    "-H",
    "--hostname",
    action="store",
    help="cPanel hostname or IP address to check license for",
    default=None,
    required=True,
)

args = parser.parse_args()

try:
    hostname = socket.gethostbyname(args.hostname)
except socket.gaierror as e:
    print(f"LICENCE WARNING - {e.strerror}")
    sys.exit(3)

url = f"{LICENSE_CHECK_URL}{hostname}"
s = requests.Session()
result = s.get(url)
parser = CPanelHTMLParser()
parser.feed(result.text)
results_dict = {}
for i in table_dict.keys():
    for j in table_dict[i].keys():
        if i == 1:
            results_dict[table_dict[i][j]] = "".strip()
        elif i == 2:
            results_dict[table_dict[i - 1][j]] = table_dict[i][j].strip()

if "Status" in results_dict and results_dict["Status"] == "active":
    print(
        f"LICENSE OK - Package: {results_dict['Package']} / Product: {results_dict['Product']} / Activity: {results_dict['Activity']}"
    )
else:
    print("LICENSE CRITICAL - Not licensed")
    sys.exit(2)
